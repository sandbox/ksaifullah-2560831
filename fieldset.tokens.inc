<?php

/**
 * @file
 * Builds placeholder replacement tokens for custom fieldset.
 */

/**
 * Implements hook_token_info().
 */
function fieldset_token_info() {

  $field_types = fieldset_field_details();

  if (count($field_types)) {
    $types = array();
    $tokens = array();

    foreach ($field_types as $field_type_name => $field_type_details) {
      if (!count($field_type_details['fields'])) continue;
      $types[$field_type_name.'-field'] = array(
        'name' => $field_type_details['label'],
        'description' => $field_type_details['description'],
        'needs-data' => $field_type_name.'-field',
      );
      foreach (fieldset_field_list($field_type_name) as $component => $component_label) {
        $tokens[$field_type_name.'-field']['component-' . $component] = array(
          'name' => t('Component: %component', array('%component' => $component_label)),
          'description' => t('The field component %component.', array('%component' => $component_label)),
        );
      }
    }
    
    $token_info = array(
      'types' => $types,
      'tokens' => $tokens,
    );

    foreach (fieldset_token_types_chained(NULL, TRUE) as $token_type => $tokens) {
      $token_info['tokens'][$token_type] = $tokens;
    }

    return $token_info;
  }
}

/**
 * Implements hook_tokens().
 */
function fieldset_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $sanitize = !empty($options['sanitize']);
  $replacements = array();

  // This handles the field tokens.
  if (isset($data[$type]) && $known_tokens = fieldset_token_types_chained($type)) {
    foreach ($tokens as $name => $original) {
      $field_type_name = substr($name, 0, strpos($name, '-'));
      
      if (in_array($field_type_name, array_keys(fieldset_field_details()))) {

        $parts = explode(':', $name);
        $field_name = array_shift($parts);
        $field_name = str_replace('-', '_', substr($field_name, strlen($field_type_name)+1));

        // Ensure that this is actually a real field token before replacing.
        // This will mimimise the chances of false matches like 'name-raw'.
        if (field_info_field($field_name)) {
          $items = field_get_items($type, $data[$type], $field_name);
          if (empty($items)) {
            $replacements[$original] = '';
            continue;
          }

          // Find the delta value.
          $delta = NULL;
          $next = array_shift($parts);
          if (isset($next)) {
            if (is_numeric($next) && ((string) intval($next)) === (string) $next) {
              $delta = $next;
            }
            elseif ($next == 'all') {
              $delta = 'all';
            }
            else {
              // Return the value to the array for the next step.
              $delta = 0;
              array_unshift($parts, $next);
            }
          }
          else {
            $delta = 0;
          }

          if ($delta != 'all' && !isset($items[$delta])) {
            $replacements[$original] = '';
            continue;
          }

          // Find the token action and format / component.
          $action = NULL;
          $action_key = NULL;
          if ($next = array_shift($parts)) {
            if (strpos($next, 'component-') === 0) {
              $action = 'component';
              $action_key = substr($next, 10);
            }
          }
          else {
            $action_key = 'default';
            $action = 'formatter';
          }

          $field_value = array();
          if ($action == 'formatter') {
            if ($delta != 'all') {
              $items = array($items[$delta]);
            }

            foreach ($items as $item) {
              $field_value[] = implode(', ', $item);
            }
          }
          else {
            if ($delta != 'all') {
              $items = array($items[$delta]);
            }
            foreach ($items as $item) {
              if (isset($item[$action_key])) {
                $field_value[] = $item[$action_key];
              }
            }
          }

          $field_value = implode('; ', array_filter($field_value));
          $replacements[$original] = $sanitize ? check_plain($field_value) : $field_value;
        }
      }
    }
  }

  /*if (in_array($type, array_keys(fieldset_field_details()) ) && !empty($data[$type])) {
    dpm($data[$type]);
    $applicant = $data[$type];
    $applicant_components = array();
    foreach (fieldset_field_list($field_type_name) as $key => $title) {
      if (!empty($applicant[$key])) {
        $applicant_components[$key] = $applicant[$key];
      }
      else {
        $applicant_components[$key] = '';
      }
    }

    foreach ($tokens as $key => $original) {
      if ($key == 'default') {
        // Full default formatted applicant.
        $default = implode(', ', $applicant_components);
        $replacements[$original] = $sanitize ? check_plain($default) : $default;
      }
      elseif (strpos($key, 'component-') === 0) {
        list(,$component) = explode('-', $key, 2);
        $replacements[$original] = $sanitize ? check_plain($applicant_components[$component]) : $applicant_components[$component];
      }
    }
  }*/

  return $replacements;
}

/**
 * Defines a list of token types that can be chained with the name field.
 *
 * @return
 *   If an entity (token) type is given, returns the chained sub-list.
 */
function fieldset_token_types_chained($type = NULL, $reset = FALSE) {
  // This functions gets called rather often when replacing tokens.
  static $drupal_static_fast;
  if (!isset($drupal_static_fast)) {
    $drupal_static_fast['types'] = &drupal_static(__FUNCTION__);
  }
  $types = &$drupal_static_fast['types'];

  if (!isset($types) || $reset) {
    // Note that this hook contains translated strings, so each language is
    // cached separately.
    global $language;
    $langcode = $language->language;
    if (!$reset && $cache = cache_get("fieldset_token_types_chained:$langcode", 'cache')) {
      $types = $cache->data;
    }

    if (!$types) {
      $types = array();
      foreach (field_info_field_map() as $field_name => $info) {
        if (in_array($info['type'], array_keys(fieldset_field_details()) )) {
          $field_type_name = $info['type'];

          foreach ($info['bundles'] as $entity_type => $bundles) {
            
            $labels = array();
            foreach ($bundles as $bundle) {
              $instance = field_info_instance($entity_type, $field_name, $bundle);
              $labels[$instance['label']] = $instance['label'];
            }
            $label = array_shift($labels);
            $clean = str_replace('_', '-', $field_name);
            if (empty($labels)) {
              $description = t('@field_type_name field in the default format. To specify a delta value, use "@token:0". Append the other chained options after the delta value like this, "@token:0:component-given". Replace the delta value with all to obtain all items in the field like this "@token:all".',
                array('@field_type_name' => $field_type_name, '@token' => $clean));
            }
            else {
              $description = t('@field_type_name field in the default format. Also known as %labels', array('%field_type_name' => $field_type_name, '%labels' => implode(', ', $labels)));
            }

            // Make sure we get the correct token type.
            $entity_info = entity_get_info($entity_type);
            $token_type = isset($entity_info['token type']) ? $entity_info['token type'] : $entity_type;
            $types[$token_type][$field_type_name.'-' . $clean] = array(
              'name' => check_plain($label),
              'description' => $description,
              'type' => $field_type_name.'-field',
            );
          }
        }
      }
      cache_set("fieldset_token_types_chained:$langcode", $types);
    }
  }
  if (isset($type)) {
    return isset($types[$type]) ? $types[$type] : NULL;
  }
  return $types;
}

