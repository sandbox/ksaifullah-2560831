<?php


/**
 * Intermediate function to delivery an array to form function
 *
 * @see fieldset_menu()
 */
function fieldset_field_type_edit($field_type_name = NULL) {
  return drupal_get_form('fieldset_field_type_form', $field_type_name);
}

/**
 * Form to add a new field type
 *
 * @see fieldset_field_type_edit()
 * @see fieldset_menu()
 */
function fieldset_field_type_form($form, &$form_state, $field_type_name = NULL) {
  $field_types = fieldset_field_details();

  if (!empty($field_type_name) AND in_array($field_type_name, array_keys($field_types))) {
    $form['field_type_name'] = array(
      '#type' => 'hidden',
      '#value' => $field_type_name,
      '#access' => FALSE,
    );
    $form['edit'] = array(
      '#markup' => '<strong>Field type: </strong>'.$field_type_name,
    );
  } else {
    $form['field_type_name'] = array(
      '#type' => 'textfield',
      '#title' => t('Field type (machine name)'),
      '#size' => 24,
      '#maxlength' => 24,
      '#required' => TRUE,
      '#description' => t('Allowable characters (a-z, 0-9) only. 24 characters max. Will prefixed by fs (e.g. applicant -> fsapplicant).'),
    );
  }

  $form['field_type_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Field title (human readable name)'),
    '#default_value' => isset($field_types[$field_type_name]['label']) ? $field_types[$field_type_name]['label'] : '',
    '#size' => 60,
    '#maxlength' => 255,
    '#required' => TRUE,
  );

  $form['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Why do you create this field?'),
    '#default_value' => isset($field_types[$field_type_name]['description']) ? $field_types[$field_type_name]['description'] : '',
  );

  $form['actions']['#weight'] = 99;
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
    '#weight' => 1,
  );
  $form['actions']['cancel'] = array(
    '#markup' => l(t('Cancel'), 'admin/structure/fieldset'),
    '#weight' => 2,
  );

  return $form;
}

/**
 * Form Validation Handler
 *
 * @see fieldset_field_type_form()
 */
function fieldset_field_type_form_validate($form, &$form_state) {

  $values = &$form_state['values'];

  $form_state['redirect'] = 'admin/structure/fieldset';
  
  $field_type_name = &$form_state['values']['field_type_name'];
  $field_type_name = strtolower(preg_replace('/[^A-Za-z0-9]/', '', $field_type_name));
  //if (!isset($form_state['complete form']['edit'])) $field_type_name = 'fs'.$field_type_name;
  if (substr($field_type_name, 0, 2) != 'fs') $field_type_name = 'fs'.$field_type_name;

  if (strlen($field_type_name) < 3) {
    form_set_error('field_type_name', t('Invalid field type.'));
  }

  $title = $values['field_type_title'];
  if (!strlen($title)) {
    form_set_error('field_type_title', t('Please input a human readable field title.'));
  }
}

/**
 * Form Submit Handler
 *
 * @see fieldset_field_type_form()
 */
function fieldset_field_type_form_submit($form, &$form_state) {
  
  $field_type = array(
    'field_type_name' => check_plain($form_state['values']['field_type_name']),
    'field_type_title' => check_plain($form_state['values']['field_type_title']),
    'description' => check_plain($form_state['values']['description']),
  );

  $status = drupal_write_record('fieldset_field_types', $field_type, (isset($form_state['complete form']['edit']) ? 'field_type_name' : array()));
  
  if ($status == 1) {
    $message = t('New Field Type @name added successfully.', array('@name' => $field_type['field_type_name']));
  }
  elseif ($status == 2) {
    $message = t('Field Type @name updated successfully.', array('@name' => $field_type['field_type_name']));
  }
  else {
    $message = t('There is something wrong with this action!');
  }

  drupal_set_message(t($message));
}

/**
 * Return a form to confirm deletion of field type
 *
 * @see fieldset_menu()
 */
function fieldset_field_type_delete($field_type_name) {
  return drupal_get_form('fieldset_field_type_delete_form', $field_type_name);
}

/**
 * Form to delete field type
 *
 * @see fieldset_field_type_delete()
 */
function fieldset_field_type_delete_form($form, &$form_state, $field_type_name) {

  if (!in_array($field_type_name, array_keys(fieldset_field_details()))) {
    $form['error'] = array(
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#attributes' => array(
        'style' => 'color: red;',
      ),
      '#value' => t('There is no such field type <strong>@type_name</strong>.', array('@type_name' => $field_type_name)),
    );

    return $form;
  }
  elseif (count(fieldset_field_active_elements($field_type_name)) > 0) {
    $form['error'] = array(
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#attributes' => array(
        'style' => 'color: red;',
      ),
      '#value' => t('The field <strong>@type_name</strong> is in use.', array('@type_name' => $field_type_name)),
    );

    return $form;
  }
  
  $form['field_type_name'] = array(
    '#type' => 'hidden',
    '#value' => $field_type_name,
    '#access' => FALSE,
  );
  
  $form['confirmation'] = array(
    '#type' => 'html_tag',
    '#tag' => 'p',
    '#value' => t('Are you sure you want delete field type <strong>@field_name</strong>. You can\'t undo this action.', array('@field_name' => $field_type_name)),
  );
  
  $form['actions']['#weight'] = 99;
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Delete field item',
    '#weight' => 1,
  );
  $form['actions']['cancel'] = array(
    '#markup' => l(t('Cancel'), 'admin/structure/fieldset'),
    '#weight' => 2,
  );

  return $form;
}

/**
 * Validation Handler
 *
 * @see fieldset_field_item_delete_form()
 */
function fieldset_field_type_delete_form_validate($form, &$form_state) {
  if (isset($form_state['values']['field_type_name'])) {
    $field_type_name = $form_state['values']['field_type_name'];
    if (count(fieldset_field_active_elements($field_type_name)) > 0) {
      form_set_error('field_type_name', t('The field <strong>@type_name</strong> is in use. You can\'t delete it.', array('@type_name' => $field_type_name)));
    }
    $form_state['redirect'] = 'admin/structure/fieldset';
  }
  else {
    form_set_error('field_type_name', t('The field <strong>@type_name</strong> is invalid.', array('@type_name' => $field_type_name)));
  }
}

/**
 * Submit Handler
 *
 * @see fieldset_field_item_delete_form()
 */
function fieldset_field_type_delete_form_submit($form, &$form_state) {
  $field_type_name = $form_state['values']['field_type_name'];

  $query = db_delete('fieldset_field_items')
    ->condition('fieldset_field_items.field_type_name', $field_type_name)
    ->execute();

  $query = db_delete('fieldset_field_types')
    ->condition('fieldset_field_types.field_type_name', $field_type_name)
    ->execute();

  foreach (fieldset_field_active_elements($field_type_name) as $field_name) {
    field_delete_field($field_name);
  }

  cache_clear_all();
}

/*************************** View Field Type *************************************/

function fieldset_field_type_view() {

  $output = array();

  $actions = array(
    'items' => array(
      l('Add field type', 'admin/structure/fieldset/add'),
    ),
    'type' => 'ul',
    'attributes' => array(
      'class' => array(
        'action-links',
      ),
    ),
  );

  $output[] = theme('item_list', $actions);

  $field_types = array(
    'header' => array(
      'Machine name',
      'Field Title',
      'Field Items',
      'Edit',
      'Delete',
    ),
  );
  
  foreach (fieldset_field_details() as $field_type_name => $field_type_details) {
    $field_types['rows'][] = array(
      $field_type_name,
      $field_type_details['label'],
      l('View', 'admin/structure/fieldset/'.$field_type_name),
      l('Edit', 'admin/structure/fieldset/'.$field_type_name.'/edit'),
      l('Delete', 'admin/structure/fieldset/'.$field_type_name.'/delete'),
    );
  }

  $output[] = theme('table', $field_types);

  return implode('',$output);
}

/************************* Field Item ********************************/

/**
 * Return a form to add  new field item
 *
 * @see fieldset_menu()
 */
function fieldset_field_item_add($field_type_name = NULL) {
  return drupal_get_form('fieldset_field_item_form', $field_type_name);
}

function fieldset_field_item_edit($field_type_name = NULL, $field_item_name = NULL) {
  return drupal_get_form('fieldset_field_item_form', $field_type_name, $field_item_name);
}

/**
 * Form to add a new item to custom field type
 *
 * @see fieldset_field_type_item_edit()
 * @see fieldset_menu()
 */
function fieldset_field_item_form($form, &$form_state, $field_type_name = NULL, $field_item_name = NULL) {

  if (!in_array($field_type_name, array_keys(fieldset_field_details()))) {
    $form['error'] = array(
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#attributes' => array(
        'style' => 'color: red;',
      ),
      '#value' => t('There is no such field type having name of <strong>@type_name</strong>.', array('@type_name' => $field_type_name)),
    );

    return $form;
  }

  $form['#attached']['js'][] = drupal_get_path('module', 'fieldset') . '/js/fieldset.min.js';

  $form['field_type_name'] = array(
    '#type' => 'hidden',
    '#value' => $field_type_name,
    '#access' => FALSE,
  );

  if ($field_item_name AND in_array($field_item_name, array_keys(fieldset_field_list($field_type_name)))) {
    $field_types = fieldset_field_details();
    $field_item_details = $field_types[$field_type_name]['fields'][$field_item_name];
    $form['field_item_name'] = array(
      '#type' => 'hidden',
      '#value' => $field_item_name,
      '#access' => FALSE,
    );
    $form['edit'] = array(
      '#type' => 'item',
      '#title' => t('Field item (machine name)'),
      '#markup' => $field_item_name,
    );
  }
  else {
    $form['field_item_name'] = array(
      '#type' => 'textfield',
      '#title' => t('Field item (machine name)'),
      '#size' => 24,
      '#maxlength' => 24,
      '#required' => TRUE,
      '#description' => t('Allowable characters (a-z, 0-9) only. 24 characters max. 2 characters min.'),
      '#weight' => 2,
    );
  }

  $form['field_item_label'] = array(
    '#type' => 'textfield',
    '#title' => t('Field label (human readable name)'),
    '#size' => 60,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#default_value' => isset($field_item_details['field_item_label']) ? $field_item_details['field_item_label'] : '',
    '#weight' => 4,
  );

  $form['form_field_type'] = array(
    '#type' => 'select',
    '#title' => t('Widget Type'),
    '#options' => fieldset_field_item_form_type(isset($field_item_details['field_item_schema']['type']) ? $field_item_details['field_item_schema']['type'] : NULL),
    '#default_value' => isset($field_item_details['field_item_form']['#type']) ? $field_item_details['field_item_form']['#type'] : 'textfield',
    '#required' => TRUE,
    '#description' => t('Type of the field as per drupal form api.'),
    '#weight' => 6,
  );
  
  $data_types = array(
    'boolean' => 'Boolean',
    'int' => 'Integer',
    'float' => 'Float',
    'varchar' => 'Text (255)',
    'text' => 'Text (long)',
  );
  $form['data_type'] = array(
    '#type' => 'select',
    '#title' => t('Data Type'),
    '#options' => $data_types,
    '#default_value' => isset($field_item_details['field_item_schema']['type']) ? $field_item_details['field_item_schema']['type'] : 'varchar',
    '#required' => TRUE,
    '#states' => array(
      'invisible' => array(
        ':input[name="form_field_type"]' => array(
          array('value' => 'checkbox'),
          array('value' => 'date'),
          array('value' => 'textarea'),
        ),
      ),
    ),
    '#weight' => 8,
    '#access' => isset($field_item_details['field_item_schema']['type']) ? FALSE : TRUE,
  );
  if (isset($field_item_details['field_item_schema']['type'])) {
    $form['data_type_display'] = array(
      '#type' => 'item',
      '#title' => t('Data Type'),
      '#markup' => t('@data_type', array('@data_type' => $data_types[$field_item_details['field_item_schema']['type']])),
      '#weight' => 9,
    );
  }

  $form['form_field_size'] = array(
    '#type' => 'textfield',
    '#title' => t('Size'),
    '#size' => 10,
    '#maxlength' => 3,
    '#default_value' => isset($field_item_details['field_item_form']['#size']) ? $field_item_details['field_item_form']['#size'] : '',
    '#description' => t('Size of the textfield in number of characters. Positive integers only (1 - 255).'),
    '#states' => array(
      'visible' => array(
        ':input[name="form_field_type"]' => array('value' => 'textfield'),
      ),
    ),
    '#weight' => 10,
  );

  $form['form_field_maxlength'] = array(
    '#type' => 'textfield',
    '#title' => t('Maxlength'),
    '#size' => 10,
    '#maxlength' => 3,
    '#default_value' => isset($field_item_details['field_item_form']['#maxlength']) ? $field_item_details['field_item_form']['#maxlength'] : '',
    '#description' => t('Allowable number of characters in a textfield. Positive integers only (1 - 255).'),
    '#states' => array(
      'visible' => array(
        ':input[name="form_field_type"]' => array('value' => 'textfield'),
      ),
    ),
    '#weight' => 12,
  );

  $options = function($options) {
    $values = array();
    foreach ($options as $key => $value) {
      $values[] = $key.'|'.$value;
    }
    return implode("\n", $values);
  };
  $form['form_field_options'] = array(
    '#type' => 'textarea',
    '#title' => t('Options'),
    '#default_value' => isset($field_item_details['field_item_form']['#options']) ? $options($field_item_details['field_item_form']['#options']) : '',
    '#description' => t('Options for Select or radios. One option per line. Use pipe to seperate key and value. If there is no pipe the whole string in one line will be used as key and value.'),
    '#states' => array(
      'visible' => array(
        ':input[name="form_field_type"]' => array(
          array('value' => 'checkboxes'),
          array('value' => 'radios'),
          array('value' => 'select'),
        ),
      ),
    ),
    '#weight' => 14,
  );

  $form['form_field_default'] = array(
    '#type' => 'textfield',
    '#title' => t('Default value'),
    '#size' => 60,
    '#maxlength' => 255,
    '#default_value' => isset($field_item_details['field_item_form']['#default_value']) ? $field_item_details['field_item_form']['#default_value'] : '',
    '#description' => t('Default value for this field. For the fields with options, please put the key, otherwise it will not be set. Checkboxes can have multiple default values, separate values with comma in that case.'),
    '#states' => array(
      'invisible' => array(
        ':input[name="form_field_type"]' => array(
          array('value' => 'checkbox'),
          array('value' => 'date'),
          array('value' => 'textarea'),
        ),
      ),
    ),
    '#weight' => 16,
  );

  $form['form_field_description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#default_value' => isset($field_item_details['field_item_form']['#description']) ? $field_item_details['field_item_form']['#description'] : '',
    '#description' => t('Description of the expected value of this field.'),
    '#weight' => 18,
  );

  $form['form_field_weight'] = array(
    '#type' => 'textfield',
    '#title' => t('Weight'),
    '#size' => 10,
    '#maxlength' => 3,
    '#default_value' => isset($field_item_details['field_item_form']['#weight']) ? $field_item_details['field_item_form']['#weight'] : '',
    '#description' => t('Positive integers only (1 - 255).'),
    '#required' => TRUE,
    '#weight' => 20,
  );

  $form['form_field_required'] = array(
    '#type' => 'checkbox',
    '#default_value' => isset($field_item_details['field_item_form']['#required']) ? $field_item_details['field_item_form']['#required'] : FALSE,
    '#title' => t('Required'),
    '#weight' => 22,
  );

  $form['form_schema_index'] = array(
    '#type' => 'checkbox',
    '#default_value' => isset($field_item_details['field_item_schema']['index']) ? $field_item_details['field_item_schema']['index'] : FALSE,
    '#title' => t('Index this field so that mysql search engine can perform better while searching in this field.'),
    '#weight' => 24,
  );

  $form['actions']['#weight'] = 99;
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
    '#weight' => 1,
  );
  $form['actions']['cancel'] = array(
    '#markup' => l(t('Cancel'), 'admin/structure/fieldset/'.$field_type_name),
    '#weight' => 2,
  );

  return $form;
}

/**
 * Form Validation Handler
 *
 * @see fieldset_field_type_form()
 */
function fieldset_field_item_form_validate($form, &$form_state) {

  $values = &$form_state['values'];

  // check a valid field type name
  if (isset($values['field_type_name']) && in_array($values['field_type_name'], array_keys(fieldset_field_details()) )) {
    $field_type_name = $values['field_type_name'];
    $form_state['redirect'] = 'admin/structure/fieldset/'.$field_type_name;
  }
  else {
    $field_type_name = '';
    form_set_error('field_type_name', 'Sorry we are unable to define the field type.');
  }
  
  // check an acceptable field item name
  $field_item_name = &$values['field_item_name'];
  $field_item_name = strtolower(preg_replace('/[^A-Za-z0-9]/', '', $field_item_name));
  
  if (strlen($field_item_name) < 2) {
    form_set_error('field_item_name', t('Invalid field item name.'));
  }
  elseif (in_array($field_item_name, array_keys(fieldset_field_list($field_type_name)) ) AND !isset($form_state['complete form']['edit'])) {
    form_set_error('field_item_name', t('Field type already exists.'));
  }

  // check there is a valid label
  $label = $values['field_item_label'];
  if (!strlen($label)) {
    form_set_error('field_item_label', t('Please input a human readable field label.'));
  }

  // Check there is a valid data type
  if (!(($values['data_type'] == 'boolean' AND $values['form_field_type'] == 'checkbox')
    OR (in_array($values['data_type'], array('int', 'float', 'varchar')) AND in_array($values['form_field_type'], array('checkboxes', 'radios', 'select', 'textfield')))
    OR ($values['data_type'] == 'varchar' AND $values['form_field_type'] == 'date')
    OR ($values['data_type'] == 'text' AND $values['form_field_type'] == 'textarea'))) {
    form_set_error('form_field_type', t('aThe combination of field widget type and data type is not matching. Please send this issue to the module maintainer.'));
  }

  // determine field item property type
  $values['property_type'] = fieldset_field_item_property_type($values['form_field_type'], $values['data_type']);
  if (!$values['property_type']) {
    form_set_error('form_field_type', t('The combination of field widget type and data type is not matching. Please send this issue to the module maintainer.'));
  }

  // check that field_size and maxlenth are valid integer
  if ($values['form_field_type'] == 'textfield') {
    foreach (array('form_field_size', 'form_field_maxlength') as $form_field) {
      $value = $values[$form_field];
      if (!is_numeric($value) || intval($value) != $value || $value < 1 || $value > 255) {
        form_set_error($form_field, t('Please input positive integer only.'));
      }
    }
  }

  // check that field_weight is a valid integer
  $value = $values['form_field_weight'];
  if (!is_numeric($value) || intval($value) != $value || $value < 1 || $value > 255) {
    form_set_error('form_field_weight', t('Please input positive integer only.'));
  }

  //check that option fields have at least two options
  if (in_array($values['form_field_type'], array('checkboxes', 'radios', 'select'))) {
    if (count(explode("\n", $values['form_field_options'])) < 2) {
      form_set_error('form_field_options', t('Please input at least two options.'));
    }
  }

}

/**
 * Form Submit Handler
 *
 * @see fieldset_field_type_form()
 */
function fieldset_field_item_form_submit($form, &$form_state) {

  $values = $form_state['values'];

  array_map('trim', $values);

  // find property type
  $data_type = $values['data_type'];
  $form_field_type = $values['form_field_type'];


  // group: property
  $field_item_property = array(
    'type' => $values['property_type'],
    'label' => check_plain($values['field_item_label']),
  );

  // group: schema
  $field_item_schema = array(
    'description' => check_plain($values['field_item_label']),
    'type' => $values['data_type'],
    'not null' => TRUE,
    'index' => $values['form_schema_index'],
  );
  if ($field_item_schema['type'] == 'varchar') {
    $field_item_schema['default'] = '';
    $field_item_schema['length'] = 255;
  }
  elseif (in_array($field_item_schema['type'], array('int', 'float', 'boolean'))) {
    $field_item_schema['default'] = 0;
  }

  // group: forms
  $field_item_form = array(
    '#type' => $values['form_field_type'],
    '#title' => check_plain($values['field_item_label']),
    '#weight' => $values['form_field_weight'],
    '#required' => $values['form_field_required'],
  );
  if (isset($values['form_field_description'])) {
    $field_item_form['#description'] = check_plain($values['form_field_description']);
  }
  switch ($field_item_form['#type']):
    case 'textfield':
      $field_item_form['#size'] = $values['form_field_size'];
      $field_item_form['#maxlength'] = $values['form_field_maxlength'];
      $field_item_form['#default_value'] = check_plain($values['form_field_default']);
      break;

    case 'checkboxes':
    case 'radios':
    case 'select':
      $raw_options = array_filter(array_map('trim', explode("\n", $values['form_field_options'])));
      array_map('check_plain', $raw_options);
      $options = array();
      foreach ($raw_options as $x) {
        $y = array_values(array_map('trim', explode('|', $x)));
        if (strlen($y[0])) $options[$y[0]] = isset($y[1]) ? $y[1] : $y[0];
        elseif (isset($y[1]) AND strlen($y[1])) $options[$y[1]] = $y[1];
      }
      $field_item_form['#options'] = $options;
      if (in_array($field_item_form['#type'], array('radios', 'select')) AND in_array($values['form_field_default'], array_keys($options))) {
        $field_item_form['#default_value'] = $values['form_field_default'];
      }
      elseif ($field_item_form['#type'] == 'checkboxes' AND count($y = explode(',',$values['form_field_default']))) {
        $x = array();
        foreach ($y as $z) {
          if (in_array($z, $options)) {
            $x[] = $z;
          }
        }
        $field_item_form['#default_value'] = $x;
      }
      break;
  endswitch;
  
  $field_item = array(
    'field_type_name' => $values['field_type_name'],
    'field_item_name' => $values['field_item_name'],
    'field_item_label' => check_plain($values['field_item_label']),
    'field_item_property' => serialize($field_item_property),
    'field_item_schema' => serialize($field_item_schema),
    'field_item_form' => serialize($field_item_form),
    'field_item_weight' => $values['form_field_weight'],
  );

  if (in_array($values['field_item_name'], array_keys(fieldset_field_list($values['field_type_name']))) AND !isset($form_state['complete form']['edit'])) {
    drupal_set_message(t('You already have a field with this name @name', array('@name' => $values['field_item_name'])));
  }
  else {
    $status = drupal_write_record('fieldset_field_items', $field_item, (isset($form_state['complete form']['edit']) ? array('field_type_name', 'field_item_name') : array()));
    if ($status == 1) {
      drupal_set_message(t('New field item <strong>@item_name</strong> added successfully.', array('@item_name' => $values['field_item_name'])));
      
      foreach (fieldset_field_active_elements($values['field_type_name']) as $field_name) {
        $data_table_name = 'field_data_'.$field_name;
        $revision_table_name = 'field_revision_'.$field_name;
        if (db_table_exists($data_table_name) AND db_table_exists($revision_table_name)) {
          db_add_field($data_table_name, $field_name.'_'.$values['field_item_name'], $field_item_schema);
          db_add_field($revision_table_name, $field_name.'_'.$values['field_item_name'], $field_item_schema);
        }
      }
      cache_clear_all();
    }
    elseif ($status ==2) {
      drupal_set_message(t('The field item <strong>@item_name</strong> has been updated successfully.', array('@item_name' => $values['field_item_name'])));
    }
    else drupal_set_message(t('Sorry! There was something wrong with the process.'));
  }
}

function fieldset_field_item_property_type($form_field_type, $data_type) {
  switch ($form_field_type):
    case 'checkbox':
      return 'list_boolean';
      break;
    case 'checkboxes':
    case 'radios':
    case 'select':
      if (in_array($data_type, array('int', 'float', 'varchar')))
        return 'list_'.$data_type;
      break;
    case 'date':
      return 'text';
      break;
    case 'textfield':
      if ($data_type == 'int') return 'number_decimal';
      elseif($data_type == 'float') return 'number_float';
      else return 'text';
      break;
    case 'textarea':
      return 'text_long';
      break;
    default:
      return NULL;
      break;
  endswitch;
}

function fieldset_field_item_form_type($data_type) {
  $form_field_types = array(
    'checkbox' => 'Checkbox',
    'checkboxes' => 'Checkboxes',
    'date' => 'Date',
    'radios' => 'Radios',
    'textfield' => 'Text Field',
    'select' => 'Select',
    'textarea' => 'Textarea',
  );
  $field_types = array();
  switch ($data_type):
    case 'boolean':
      $field_types['checkbox'] = 'Checkbox';
      break;
    case 'varchar':
      $field_types['date'] = 'Date';
    case 'float':
    case 'int':
      $field_types['checkboxes'] = 'Checkboxes';
      $field_types['radios'] = 'Radios';
      $field_types['textfield'] = 'Text Field';
      $field_types['select'] = 'Select';
      break;
    case 'text':
      $field_types['textarea'] = 'Textarea';
      break;
    default:
      $field_types = $form_field_types;
  endswitch;
  return $field_types;
}

/*************************** View Field items *************************************/

function fieldset_field_item_view($field_type_name) {

  if (!in_array($field_type_name, array_keys(fieldset_field_details()))) {
    $message = array(
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#attributes' => array(
        'style' => 'color: red;',
      ),
      '#value' => t('There is no field type having name of <strong>@name</strong>.', array('@name' => $field_type_name)),
    );
    return drupal_render($message);
  }

  $output = array();

  $actions = array(
    'items' => array(
      l('Add field item', 'admin/structure/fieldset/'.$field_type_name.'/add'),
    ),
    'type' => 'ul',
    'attributes' => array(
      'class' => array(
        'action-links',
      ),
    ),
  );

  $output[] = theme('item_list', $actions);

  $field_items = array(
    'header' => array(
      'Machine name',
      'Item label',
      'Properties',
      'Schema type',
      'Form Properties',
      'Edit',
      'Delete',
    ),
  );
  
  $field_types = fieldset_field_details();
  foreach ($field_types[$field_type_name]['fields'] as $field_item_name => $field_item_details) {
    $field_items['rows'][] = array(
      $field_item_name,
      $field_item_details['field_item_label'],
      theme('fieldset_item_properties', array('items' => $field_item_details['field_item_property'])),
      theme('fieldset_item_properties', array('items' => $field_item_details['field_item_schema'])),
      theme('fieldset_item_properties', array('items' => $field_item_details['field_item_form'])),
      l('Edit', 'admin/structure/fieldset/'.$field_type_name.'/'.$field_item_name.'/edit'),
      l('Delete', 'admin/structure/fieldset/'.$field_type_name.'/'.$field_item_name.'/delete'),
    );
  }

  $output[] = theme('table', $field_items);

  return implode('',$output);
}


/**
 * Delete a field item
 *
 * @see fieldset_menu
 */
function fieldset_field_item_delete($field_type_name, $field_item_name) {
  return drupal_get_form('fieldset_field_item_delete_form', $field_type_name, $field_item_name);
}

/**
 * Form to delete field item
 *
 * @see fieldset_field_item_delete()
 */
function fieldset_field_item_delete_form($form, &$form_state, $field_type_name, $field_item_name) {

  if (!in_array($field_type_name, array_keys(fieldset_field_details()))) {
    $form['error'] = array(
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#attributes' => array(
        'style' => 'color: red;',
      ),
      '#value' => t('There is no such field having name <strong>@item_name</strong> in field type <strong>@type_name</strong>.', array('@item_name' => $field_item_name, '@type_name' => $field_type_name)),
    );

    return $form;
  }
  elseif (count(fieldset_field_list($field_type_name)) < 2 AND count(fieldset_field_active_elements($field_type_name)) > 0) {
    $form['error'] = array(
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#attributes' => array(
        'style' => 'color: red;',
      ),
      '#value' => t('The field <strong>@type_name</strong> is in use. You have to keep at least one item for this.', array('@type_name' => $field_type_name)),
    );

    return $form;
  }
  
  $form['field_type_name'] = array(
    '#type' => 'hidden',
    '#value' => $field_type_name,
    '#access' => FALSE,
  );
  
  $form['field_item_name'] = array(
    '#type' => 'hidden',
    '#value' => $field_item_name,
    '#access' => FALSE,
  );
  
  $form['confirmation'] = array(
    '#type' => 'html_tag',
    '#tag' => 'p',
    '#value' => t('Are you sure you want delete form item <strong>@item_name</strong> from field type <strong>@field_name</strong>. You can\'t undo this action.', array('@item_name' => $field_item_name, '@field_name' => $field_type_name)),
  );
  
  $form['actions']['#weight'] = 99;
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Delete field item',
    '#weight' => 1,
  );
  $form['actions']['cancel'] = array(
    '#markup' => l(t('Cancel'), 'admin/structure/fieldset/'.$field_type_name),
    '#weight' => 2,
  );

  return $form;
}

/**
 * Validation Handler
 *
 * @see fieldset_field_item_delete_form()
 */
function fieldset_field_item_delete_form_validate($form, &$form_state) {
  if (isset($form_state['values']['field_type_name'])) {
    $field_type_name = $form_state['values']['field_type_name'];
    if (count(fieldset_field_list($field_type_name)) < 2 AND count(fieldset_field_active_elements($field_type_name)) > 0) {
      form_set_error('field_item_name', t('The field <strong>@type_name</strong> is in use. You have to keep at least one item for this.', array('@type_name' => $field_type_name)));
    }
    $form_state['redirect'] = 'admin/structure/fieldset/'.$field_type_name;
  }
  else {
    form_set_error('field_type_name', t('The field <strong>@type_name</strong> is invalid.', array('@type_name' => $field_type_name)));
  }
}

/**
 * Submit Handler
 *
 * @see fieldset_field_item_delete_form()
 */
function fieldset_field_item_delete_form_submit($form, &$form_state) {
  $field_type_name = $form_state['values']['field_type_name'];
  $field_item_name = $form_state['values']['field_item_name'];

  $query = db_delete('fieldset_field_items')
    ->condition('fieldset_field_items.field_type_name', $field_type_name)
    ->condition('fieldset_field_items.field_item_name', $field_item_name)
    ->execute();

  foreach (fieldset_field_active_elements($field_type_name) as $field_name) {
    $data_table_name = 'field_data_'.$field_name;
    $revision_table_name = 'field_revision_'.$field_name;
    if (db_table_exists($data_table_name) AND db_table_exists($revision_table_name)) {
      db_drop_field($data_table_name, $field_name.'_'.$field_item_name);
      db_drop_field($revision_table_name, $field_name.'_'.$field_item_name);
    }
  }
  cache_clear_all();
}

/**
 * List of active custom fields in different entity
 *
 * @param $field_type_name: string
 */
function fieldset_field_active_elements($field_type_name) {
  $query = db_select('field_config')
    ->fields('field_config', array('field_name'))
    ->condition('field_config.module', 'fieldset')
    ->condition('field_config.type', $field_type_name)
    ->condition('field_config.deleted', 0);
  $result = $query->execute();
  return $result->fetchCol();
}